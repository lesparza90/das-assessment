# das-assessment
An NodeJS Testing Framework

### Installation and Execution
Requires [Node.js](https://nodejs.org/) v8+ to run.

Clone this repository and install the dependencies

```sh
$ git clone <repo>
$ npm install
$ npm i -g testcafe
```

Run your Test in chrome
```sh
$ npm run chrome
```

Run your Test in headless mode
```sh
$ npm run chrome:headless
```

Run your Test in FF
```sh
$ npm run firefox
```

**Happy Testing!**

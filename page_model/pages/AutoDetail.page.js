import { Selector, t } from 'testcafe'

class AutoDetailPage {
  constructor () {
    this.buyNowButton = Selector('a').withText('BUY NOW')
    this.contactUsButton = Selector('a').withText('CONTACT US')
    this.infoContainer = Selector('.info')
    this.header = Selector('#m h1:nth-child(2)')
    this.infoSectionHeader = Selector('#main h2')
    this.conditionText = Selector('div.row').withText('Condition').child('.val')
    this.vinText = Selector('div.row').withText('VIN').child('.val')
    this.odometerText = Selector('div.row').withText('Odometer').child('.val')
    this.exteriorInteriorText = Selector('div.row').withText('Exterior/Interior').child('.val')
    this.sellerText = Selector('div.row').withText('Seller').child('.val')
    this.locationText = Selector('div.row').withText('Location').child('.val')
    this.carouselContainer = Selector('.svfy_viewer.svfy_sma')
    this.mainCarouselImageContainer = Selector('.svfy_render.svfy_cnt')
    this.imagesListContainer = Selector('.svfy_scroller')
    this.imagesListImages = Selector('.svfy_scroller div')
    this.categoryListContainer = Selector('.svfy_cats')
    this.nextButton = Selector('.svfy_a_next')
    this.prevButton = Selector('.svfy_a_prev')
    this.mainImage = Selector('img.svfy_img')
    this.carriageButton = Selector('a').withText('carriage')
    this.dasLogoImage = Selector('#lg')
  }

  async getCondition() {
    let condition = await this.conditionText.innerText
    return parseFloat(condition.replace('CR', ''))
  }

  async getCurrentImageUrl() {
    return await this.mainImage.getAttribute('src')
  }
}

export default new AutoDetailPage()

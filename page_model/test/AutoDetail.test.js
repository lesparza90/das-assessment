import AutoDetailPage from '../pages/AutoDetail.page'
import checkPropTypes from '../utils/propTypes.util'
import { ClientFunction } from 'testcafe'

fixture('Automobile Detail Page Tests')
  // Go to Automobile Detail Page
  .page`https://threesixtyviewer.z5.web.core.windows.net/QaAssesmentAdv.html`
  .beforeEach(() => checkPropTypes());

test('Verify user is able to buy a car', async (t) => {
  // Buy Now button is displayed
  await t.expect(AutoDetailPage.buyNowButton.visible).ok('BUY NOW button was not displayed')
  // 2. Click on BUY NOW button
  await t
        .setNativeDialogHandler(() => true)
        .click(AutoDetailPage.buyNowButton)
  // valudate User is taking to checkout page
  const getURL = ClientFunction(() => document.location.href)
  await t.expect(getURL()).contains('/checkout', 'user was not taken to cheackout page after clicking BUY NOW button')
  // 3. Fill out checkout information and Click on CHECKOUT button
  // ...
  
})

test('Verify user is able to Contact the Dealer', async (t) => {
  // Contact Us button is displayed
  await t.expect(AutoDetailPage.contactUsButton.visible).ok('CONTACT US button was not displayed')
  // 2. Click on Contact Us button
  await t.click(AutoDetailPage.contactUsButton)
  // valudate User is taking to Contact Uspage
  const getURL = ClientFunction(() => document.location.href)
  await t.expect(getURL()).contains('/contact-us', 'user was not taken to contact ud page after clicking BUY NOW button');
  // 3. Fill out the contact information and Click on SEND button
  // ...
})

test('Verify Automobile is displayed with the proper information', async (t) => {
  // Information section is displayed
  await t.expect(AutoDetailPage.infoContainer.visible).ok('Info container button was not displayed')
  // Check Header (H1) is displayed with the proper information
  const vin = await AutoDetailPage.vinText.innerText
  const VIN = require(`../data/vins/${vin}`)
  // // Check H2 is displayed with the proper information
  const expectedH1 = `${VIN.INFO.YEAR} ${VIN.INFO.MAKE} ${VIN.INFO.MODEL}`
  await t.expect(AutoDetailPage.header.innerText).eql(expectedH1, 'VIN number and header do not match')
  // Check H2 is displayed with the proper information
  const expectedH2 = `${VIN.INFO.YEAR} ${VIN.INFO.MAKE} ${VIN.INFO.MODEL} From $103000`
  await t.expect(AutoDetailPage.header.innerText).eql(expectedH2, 'VIN number and H2 do not match')
  // Validate Condition is displayed with correct info
  let condition = await AutoDetailPage.getCondition()
  await t.expect(condition).within(1, 5, 'conditions should be a number between 1 and 5')
  // Verify VIN field is displayed valid VIN number 
  let vinCount = (await AutoDetailPage.vinText.innerText).length
  await t.expect(vinCount).eql(17, 'VIN number should contain a string of 17 characters')
  // Check Odometer is displayed with valid information
  const odometer = (await AutoDetailPage.odometerText.innerText).split(' ')
  await t.expect(parseInt(odometer[0])).typeOf('number', 'first odometer value should be a number')
  await t.expect(odometer[1]).eql('mi', 'Second odometer value should be "mi"')
  // Validate Exterior/Interior is shown with proper data
  await t.expect(AutoDetailPage.exteriorInteriorText.innerText).notEql('', 'Interioir/Exterior filed is empty')
  // Verify Seller field is displayed
  await t.expect(AutoDetailPage.sellerText.innerText).notEql('', 'Seller filed is empty')
  // Check Location field is shown
  await t.expect(AutoDetailPage.locationText.innerText).notEql('', 'Location filed is empty')
})

test('Verify User is able to interact with the carousel', async (t) => {
  await t.expect(AutoDetailPage.carouselContainer.visible).ok('Carousel is not present')
  await t.expect(AutoDetailPage.mainCarouselImageContainer.visible).ok('Main carousel image is not present')
  await t.expect(AutoDetailPage.imagesListContainer.visible).ok('Carousel image list is not present')
  await t.expect(AutoDetailPage.categoryListContainer.visible).ok('Category list container is not present')

  await t.expect(AutoDetailPage.nextButton.visible).ok('Next button is not present')
  await t.expect(AutoDetailPage.prevButton.visible).ok('Prev button is not present')
  await t.click(AutoDetailPage.prevButton)
  let firtImage = await AutoDetailPage.getCurrentImageUrl()
  await t.click(AutoDetailPage.prevButton)
  await t.wait(500)
  let prevImage = await AutoDetailPage.getCurrentImageUrl()
  await t.expect(firtImage).notEql(prevImage, 'Prev image was not displayed after clicking Prev button')
  // click next buuton
  await t.click(AutoDetailPage.nextButton)
  await t.wait(500)
  let currentImage = await AutoDetailPage.getCurrentImageUrl()
  await t.expect(firtImage).eql(currentImage, 'Next image was not displayed after clicking Prev button')
  let numOfImagesBeforeFilter = await AutoDetailPage.imagesListImages.count
  await t.click(AutoDetailPage.carriageButton)
  await t.wait(500)
  let numOfImagesAfterFilter = await AutoDetailPage.imagesListImages.count
  await t.expect(numOfImagesBeforeFilter).notEql(numOfImagesAfterFilter, 'filter not applied after clicking carriage filter')
})

test('Verify User is able to interact with the carousel in Fullscreen mode', async (t) => {
  await t.click(AutoDetailPage.carouselContainer)

  await t.expect(AutoDetailPage.mainCarouselImageContainer.visible).ok('Main carousel image is not present')
  await t.expect(AutoDetailPage.imagesListContainer.visible).ok('Carousel image list is not present')
  await t.expect(AutoDetailPage.categoryListContainer.visible).ok('Category list container is not present')

  await t.expect(AutoDetailPage.nextButton.visible).ok('Next button is not present')
  await t.expect(AutoDetailPage.prevButton.visible).ok('Prev button is not present')
  await t.click(AutoDetailPage.prevButton)
  let firtImage = await AutoDetailPage.getCurrentImageUrl()
  await t.click(AutoDetailPage.prevButton)
  await t.wait(500)
  let prevImage = await AutoDetailPage.getCurrentImageUrl()
  await t.expect(firtImage).notEql(prevImage, 'Prev image was not displayed after clicking Prev button')
  // click next buuton
  await t.click(AutoDetailPage.nextButton)
  await t.wait(500)
  let currentImage = await AutoDetailPage.getCurrentImageUrl()
  await t.expect(firtImage).eql(currentImage, 'Next image was not displayed after clicking Prev button')
  let numOfImagesBeforeFilter = await AutoDetailPage.imagesListImages.count
  await t.click(AutoDetailPage.carriageButton)
  await t.wait(500)
  let numOfImagesAfterFilter = await AutoDetailPage.imagesListImages.count
  await t.expect(numOfImagesBeforeFilter).notEql(numOfImagesAfterFilter, 'filter not applied after clicking carriage filter')
})

test('Verify user is able to navigate to Home page from Automobile Detail Page', async (t) => {
  await t.expect(AutoDetailPage.dasLogoImage.visible).ok()

  const getRequestResult = ClientFunction(url => {
    return new Promise(resolve => {
        var xhr = new XMLHttpRequest();

        xhr.open('GET', url);

        xhr.onload = function () {
            resolve(xhr.status);
        };

        xhr.send(null);
    })
  })

  const imageUrl = await AutoDetailPage.dasLogoImage.getAttribute('src')
  const status = await getRequestResult(imageUrl)

  await t.expect(status).eql(200,'DAS logo image is broken')
  await t.click(AutoDetailPage.dasLogoImage)
  await t.expect(getURL()).contains('https://digitalairstrike.com/', 'user was not taken to DAS Home page after clicking DAS logo image')
})
